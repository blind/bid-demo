package com.example.bid.controller;

import com.daaao.bid.policy.redis.RedisIdTemplate;
import com.daaao.bid.policy.redis.SimpleDateFormatter;
import com.daaao.bid.policy.uid.UidGenerator;
import com.example.bid.config.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class DemoController {


    @Resource(name = "zzRedisIdTemplate")
    private RedisIdTemplate zzRedisIdTemplate;

    @Resource(name = "gysRedisIdTemplate")
    private RedisIdTemplate gysRedisIdTemplate;

    @Resource(name = "jsRedisIdTemplate")
    private RedisIdTemplate jsRedisIdTemplate;

    @Resource(name = "fullRedisIdTemplate")
    private RedisIdTemplate fullRedisIdTemplate;

    @Resource
    private UidGenerator cachedUidGenerator;

    @Resource
    private UidGenerator defaultUidGenerator;

    @RequestMapping("/test")
    public String test() {
        return "hello, there";
    }

    @RequestMapping("/bid/get")
    public String get() {
        return Bid.nextId();
    }

    @RequestMapping("/bid/get2")
    public String get2() {
        System.out.println("id6====" + zzRedisIdTemplate.next());
        System.out.println("id7====" + gysRedisIdTemplate.next());
        System.out.println("id8====" + jsRedisIdTemplate.next());
        System.out.println("id9====" + fullRedisIdTemplate.next());

        //调用
        String id5 = fullRedisIdTemplate.next("ROLEID4", SimpleDateFormatter.FORMATTER_MILLISECOND, 6, 5, 100L, false, true, null);

        return Bid.nextId("test");
    }

    @RequestMapping("/bid/get3")
    public String get3() {
        long uid = cachedUidGenerator.getUID();
        return String.valueOf(uid);
    }

    @RequestMapping("/bid/get4")
    public String get4() {
        long uid = defaultUidGenerator.getUID();
        return String.valueOf(uid);
    }
}
