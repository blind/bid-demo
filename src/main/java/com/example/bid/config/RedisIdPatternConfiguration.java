package com.example.bid.config;


import com.daaao.bid.policy.fallback.IdFallback;
import com.daaao.bid.policy.redis.IdPatternConfiguration;
import com.daaao.bid.policy.redis.RedisIdTemplate;
import com.daaao.bid.policy.redis.SimpleDateFormatter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author hao
 * @version 1.0
 * @date 2021/1/19 14:30
 */
@Configuration
public class RedisIdPatternConfiguration {

    @Bean
    public RedisIdTemplate fullRedisIdTemplate() {
//        全部参数
        IdPatternConfiguration redisIdConfig = IdPatternConfiguration.builder()
                //redis的key名称，同时也是ID前缀（如果需要拼接前缀的话）
                .key("DEMO12")
                //日期格式，不传默认不拼接日期
                .dateTimeFormatter(SimpleDateFormatter.FORMATTER_DAY)
                //redis生成ID的位数，如果不配置默认补位，如配置5，此时Redis序列值为“6”小于5位数，则生成的ID为00006
                .digits(6)
                //redis递增步长，如当前序列值为5，配置increment=5则下次序列为10
                .increment(1)
                //redis序列初始值，不配置不覆盖redis的值，若配置值小于redis服务器序列值，则不覆盖，大于则从initial的设置的值开始递增
                .initial(0L)
                //是否需要前缀，搭配key使用，默认false不拼接前缀
                .needPrefix(false)
                //是否需要每天重置序列，默认true，必须搭配配置dateTimeFormatter参数一起使用，为true的话序列会每天重置为initial的参数值，false不重置
                .isResetDaily(true)
                //ID生成的服务降级策略，当Redis不能提供服务或其他异常错误导致生成失败时ID如何生成，默认降级策略为随机数+UUID，具体看源码
                .idFallback(new IdFallback() {
                    @Override
                    public String nextId() {
                        return "id11111";
                    }
                })
                .build();
        return new RedisIdTemplate(redisIdConfig);
    }

    @Bean
    public RedisIdTemplate zzRedisIdTemplate() {
//        字段“组织编码”由系统自动生成。编码规则为：zz+年月日（20190901）+三位数（001）
        IdPatternConfiguration redisIdConfig = IdPatternConfiguration.builder()
                .key("zz")
                .dateTimeFormatter(SimpleDateFormatter.FORMATTER_DAY)
                .digits(3)
                .build();
        return new RedisIdTemplate(redisIdConfig);
    }

    @Bean
    public RedisIdTemplate gysRedisIdTemplate() {
//        供应商编码：系统自动生成，编码规则：gys+年月日（20190908）+四位数（0001）
        IdPatternConfiguration redisIdConfig = IdPatternConfiguration.builder()
                .key("gys")
                .dateTimeFormatter(SimpleDateFormatter.FORMATTER_DAY)
                .digits(4)
                .build();
        return new RedisIdTemplate(redisIdConfig);
    }

    @Bean
    public RedisIdTemplate jsRedisIdTemplate() {
//        角色编码：js+yyyyMMddHHmmssSSS
        IdPatternConfiguration redisIdConfig = IdPatternConfiguration.builder()
                .key("js")
                .dateTimeFormatter(SimpleDateFormatter.FORMATTER_MILLISECOND)
                .digits(2)
                .build();
        return new RedisIdTemplate(redisIdConfig);
    }

}
