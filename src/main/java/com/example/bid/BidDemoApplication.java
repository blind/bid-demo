package com.example.bid;

import com.daaao.bid.annotation.EnableBid;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableBid
@SpringBootApplication
public class BidDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BidDemoApplication.class, args);
    }

}
